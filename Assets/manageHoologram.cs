using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class manageHoologram : MonoBehaviour
{
    public GameObject Biologist;
    public bool open;
    public float time;
    // Start is called before the first frame update
    void Awake()
    {
        Biologist.SetActive(false);
        open = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(open)
        {
            Biologist.SetActive(true);

            time += Time.deltaTime;
        }

        if(time > 15)
        {
            Destroy(this.gameObject);
        }

    }
    public void openBiologiste()
    {
        open = true;
    }
}
