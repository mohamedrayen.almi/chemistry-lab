using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FillMilk : MonoBehaviour
{
    public Shader baseShader;
    public Shader DropColors;
    private Material material;
    private Material materialColors;
    private bool isFill; 
    private bool matupdated;
    // Start is called before the first frame update
    void Start()
    {
        isFill = false;
        matupdated = false;
        Material mat = new Material(baseShader);

        materialColors = new Material(DropColors);

        GetComponent<MeshRenderer>().material = mat;
        material = GetComponent<MeshRenderer>().material;
    }

    void changeMaterialColorsDrop()
    {
            GetComponent<MeshRenderer>().material = materialColors;
            material = GetComponent<MeshRenderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
       
            if (isFill)
            {
                if (!matupdated)
                {
                    changeMaterialColorsDrop();
                    matupdated = true;
                }
            }

            if(matupdated) 
            {
            if(material.GetFloat("_RedPresent") + material.GetFloat("_YellowPresent") + material.GetFloat("_BleuPresent") ==3 )
            {
                material.SetFloat("_FillMultiColors", material.GetFloat("_FillMultiColors") - 0.0005f );
            }
            }
        
    }

    private void OnTriggerEnter(Collider c)
    {

        if (c.gameObject.tag.Equals("Milk") && c.gameObject.GetComponent<MeshRenderer>().material.color == Color.white && isFill==false )
        {
            Destroy(c.gameObject);
            material.SetFloat("_Fill", material.GetFloat("_Fill") + 0.001f );
            if(material.GetFloat("_Fill")>=0.4f)
            {
                isFill=true;
            }
        }

        if(c.gameObject.tag.Equals("Milk"))
        {
            Destroy(c.gameObject);
        }

        
            if (isFill)
            {
               // changeMaterialColorsDrop();

                Color c1 = c.gameObject.GetComponent<MeshRenderer>().material.color;

            if(c1.r == 1.0f && c1.g == 0.0f && c1.b == 0.0f && c1.a == 0.0f)
            {
                material.SetFloat("_RedPresent", 1);
            }

            if (c1.r == 1.0f && c1.g == 1.0f && c1.b == 0.0f && c1.a == 0.0f)
            {
                // open yellow
                material.SetFloat("_YellowPresent", 1);
            }

            if (c1.r == 0.0f && c1.g == 0.0f && c1.b == 1.0f && c1.a == 0.0f)
            {
                // bleu
                material.SetFloat("_BleuPresent", 1);
            }


        }
        
    }
}
