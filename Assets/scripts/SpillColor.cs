using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpillColor : MonoBehaviour
{
    public GameObject dropColor;
    public Color matColor;
    private Material newMaterial; 
    // Start is called before the first frame update
    void Start()
    {
        dropColor.transform.localScale = new Vector3 (4.0f, 4.0f, 4.0f);

        newMaterial = new Material(Shader.Find("Universal Render Pipeline/Lit"));

        // Set the color to red
        newMaterial.color = matColor;

        dropColor.GetComponent<MeshRenderer>().material = newMaterial;
        //dropColor.GetComponent<MeshRenderer>().material.color = Color.red;
    }

    void InstantiatePrefab()
    {
        newMaterial.color = matColor;

        dropColor.GetComponent<MeshRenderer>().material = newMaterial;
        // Instancie le pr�fab � la position (0, 0, 0) et avec une rotation de z�ro
        Instantiate(dropColor, new Vector3(transform.position.x, transform.position.y + 0.005f, transform.position.z), Quaternion.identity);
    }
    // Update is called once per frame
    void Update()
    {
        if (Vector3.Angle(Vector3.down, transform.forward) > 90)
        {

            InstantiatePrefab();
        }
    }
}
