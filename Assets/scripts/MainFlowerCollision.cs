using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainFlowerCollision : MonoBehaviour
{
    public bool isCollidiing;
    // Start is called before the first frame update
    void Start()
    {
        isCollidiing = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision c)
    {
        if (c.gameObject.tag.Equals("VerserWater"))
        {
            isCollidiing = true;
        }
    }

    private void OnCollisionExit(Collision c)
    {
        if (c.gameObject.tag.Equals("VerserWater"))
        {
            isCollidiing = false;

        }
    }
}
