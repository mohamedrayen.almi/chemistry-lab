using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeatUp : MonoBehaviour
{
    public Renderer liquid;
    public ParticleSystem smokePuff;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnParticleCollision(GameObject other)
    {
        if (other.tag =="Fire")
        {
            Color currentSideCol = liquid.material.GetColor("_SideColor");
            Color currentTopCol = liquid.material.GetColor("_TopColor");
            Color newTopCol = new Color();
            if (other.gameObject.tag == "Fire" && other.gameObject.tag == "Fe" && (currentTopCol.g < Color.green.g))
            {
                smokePuff.Play();
                if (currentSideCol.r > Color.green.r)
                {
                    newTopCol.r = currentSideCol.r - 0.001f;
                }
                if (currentSideCol.g < Color.green.g)
                {
                    newTopCol.g = currentSideCol.g + 0.001f;
                }
                if (currentSideCol.b > Color.green.b)
                {
                    newTopCol.b = currentSideCol.b - 0.001f;
                }
                liquid.material.SetColor("_SideColor", newTopCol);
                liquid.material.SetColor("_TopColor", newTopCol);
            }
        }
    }
}
