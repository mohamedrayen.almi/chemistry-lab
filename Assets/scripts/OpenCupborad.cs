using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenCupborad : MonoBehaviour
{
    public bool isRigth;
    public bool isOpen;
    public int i;
    public int dirc;
    // Start is called before the first frame update
    void Start()
    {
        isOpen = false;
        if(isRigth==true)
        {
            dirc = 1;
        }
        else
        {
            dirc = -1;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if( isOpen  && i>0)
        {
            transform.Translate(0,-0.01f * dirc,0);
            i--;
        }
        else if(!isOpen && i<80)
        {
            transform.Translate(0, 0.01f * dirc, 0);
            i++;
        }
    }

    public void changeStateDoor() 
    {
        isOpen = !isOpen;
    }
}
