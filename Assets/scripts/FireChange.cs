using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class FireChange : MonoBehaviour
{
    public GameObject a;
    public GameObject b;
    public BoxCollider c;
    public GameObject d;
    public GameObject e;
    public bool k = false;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (k == true)
        {
            a.SetActive(false);
            b.SetActive(true);
        }
        else
        {
            a.SetActive(true);
            b.SetActive(false);

        }
        
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Flask"))
        {
            Debug.Log("Collision FLask Stove entered");
            k = true;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Flask"))
        {
            k = true;
        }

    }
}

