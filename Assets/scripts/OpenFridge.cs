using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenFridge : MonoBehaviour
{
    public bool isOpen;
    public int i;
    // Start is called before the first frame update
    void Start()
    {
        isOpen = false;
    }

    // Update is called once per frame
    void Update()
    {
         if(isOpen && i >=0)
        {
            transform.Rotate(0,-0.8f,0); 
            i--;
        }
        else if(i<=120 && isOpen==false)
        {
            transform.Rotate(0,0.8f,0);
            i++;
        }
    }
    
    public void OpenIsOpen()
    {
        isOpen= !isOpen;
    }
}
