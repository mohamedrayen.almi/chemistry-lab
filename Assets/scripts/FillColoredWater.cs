using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FillColoredWater : MonoBehaviour
{
    public Shader mainShader;
    private Material material;
    public Color WaterColor;


    // Start is called before the first frame update
    void Start()
    {
        Material mat = new Material(mainShader);

        GetComponent<MeshRenderer>().material = mat;
        material = GetComponent<MeshRenderer>().material;
        material.SetFloat("_Fill", 0.3f);

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.tag.Equals("Water"))
        {
            WaterColor = c.gameObject.GetComponent<MeshRenderer>().material.color;
            material.SetColor("_ColorLiquide" ,c.gameObject.GetComponent<MeshRenderer>().material.color);
            material.SetFloat("_Fill", material.GetFloat("_Fill") + 0.0005f);
        }
    }
}
