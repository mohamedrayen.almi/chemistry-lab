using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageGrowMold : MonoBehaviour
{
    public float temp;
    public bool wet;
    public bool inLight;
    public bool onAir;
    public bool onFridge;
    public bool onMicrowave;

    private Material mat; 
    // Start is called before the first frame update
    void Start()
    {
        onAir = true;
        inLight = true;
        mat = GetComponent<MeshRenderer>().material;
    }

    int CalculateSpeedGrowMold()
    {
        int i =1 ;
        if (!inLight)
        {
            i++; 
        }

        if(!onAir) 
        {
            i++;
        }

        if(wet) 
        {
            i += 6; 
        }

        if (onFridge) 
        {
            i--;
        }

        if (onMicrowave) 
        {
            i++;
        }
        return i; 
    }

    // Update is called once per frame
    void Update()
    {
        mat.SetFloat("_Mold", mat.GetFloat("_Mold") + ( CalculateSpeedGrowMold() * 0.00001f )  );
    }

    private void OnCollisionEnter(Collision c)
    {
        if(c.gameObject.name.Equals("microwve"))
        {
            onMicrowave = true;
        }
        if(c.gameObject.name.Equals("Fridge"))
        {
            onFridge = true;
        }
        if (c.gameObject.name.Equals("cupborad"))
        {
            inLight = false;
            onAir = false;
        }
        if(c.gameObject.name.Equals("dish_water"))
        {
            wet = true;
        }
    }

    private void OnCollisionExit(Collision c)
    {
        if (c.gameObject.name.Equals("microwve"))
        {
            onMicrowave = false;
        }
        if (c.gameObject.name.Equals("Fridge"))
        {
            onFridge = false;
        }
        if (c.gameObject.name.Equals("cupborad"))
        {
            inLight = true;
            onAir = true;
        }
    }
}
