using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class Drawer : MonoBehaviour
{
    public bool SelectEntered;
    public GameObject a;
    public GameObject b;
    public bool ouvert = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!ouvert)
        {
            //a.transform.rotation = Quaternion.identity;
            a.transform.position = new Vector3(15.21f- 0.1194047f, -0.02999997f - 0.112165f, 3.801729f + -0.3561581f);
            b.SetActive(false);
        }
        else if (ouvert)
        {
            float x = 0.35f;
            a.transform.position = new Vector3(x,0,0);
            b.SetActive(true);
        }

    }
    public void ouvrir()
    {
        ouvert = true;
    }
    public void fermer()
    {
        ouvert = false;
    }
    public void OPEN()
    {
        if (!SelectEntered)
        {
            ouvrir();
            SelectEntered = true;
            return;
        }
        else
        {
            fermer();
            SelectEntered = false;
            return;
        }
    }
}
