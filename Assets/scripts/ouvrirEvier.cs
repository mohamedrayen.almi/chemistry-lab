using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ouvrirEvier : MonoBehaviour
{
    public GameObject g;
    public GameObject b;
    public bool ouvert = false;
    public bool SelectEntered = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!ouvert)
        {
            g.transform.rotation = Quaternion.identity;
            b.SetActive(false);
        }
        else if (ouvert)
        {
            g.transform.rotation = Quaternion.AngleAxis(-45, Vector3.up);
            b.SetActive(true);

        }
    }
    public void ouvrir()
    {
        ouvert = true;
    }
    public void fermer()
    {
        ouvert = false;
    }
    public void WaterOn()
    {
        if (!SelectEntered)
        {
            ouvrir();
            SelectEntered = true;
            return;
        }
        else
        {
            fermer();
        SelectEntered = false;
            return;
        }
    }
}
