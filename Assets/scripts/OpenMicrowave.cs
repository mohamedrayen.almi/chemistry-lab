using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenMicrowave : MonoBehaviour
{
    public bool isOpen;
    public int i ;
    // Start is called before the first frame update
    void Start()
    {
        isOpen = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(isOpen && i >=0)
        {
            transform.Rotate(0,0,-0.8f); 
            i--;
        }
        else if(i<=120 && isOpen==false)
        {
            transform.Rotate(0,0,0.8f);
            i++;
        }


    }

    public void ChangeIsOpen()
    {
        isOpen = !isOpen;
    }
}
