using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class microscope : MonoBehaviour
{
    public bool SelectEntered;
    public Camera a;
    public Camera b;
    public bool ouvert = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
       
    {
        if (Input.GetButtonDown("Cancel")) {
            ouvert = false;
            a.enabled=false;
            b.enabled=true;
        }
        if(ouvert)
        {
            a.enabled=true ;
            b.enabled=false ;
        }
        
        
    }
    public void ouvrir()
    {
        ouvert = true;
    }
    public void fermer()
    {
        ouvert = false;
    }
    public void OPEN()
    {
        if (!SelectEntered)
        {
            ouvrir();
            SelectEntered = true;
            return;
        }
        else
        {
            fermer();
            SelectEntered = false;
            return;
        }
    }
}
