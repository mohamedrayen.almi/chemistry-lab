using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class Instiate : XRSimpleInteractable
{
    public Molecule moleculeData;
    public GameObject atom;
    GameObject boom;
    idk leScript;
    private void Start()
    {
       leScript = GetComponent<idk>();
    }
    protected override void OnSelectEntered(SelectEnterEventArgs args)
    { 
        boom=leScript.SpawnAtom(atom,transform.position);
        boom.GetComponent<MolleculeJoint>().mData = moleculeData;
    }
    
}
