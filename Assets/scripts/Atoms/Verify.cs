using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit.UI.BodyUI;

public class Verify : MonoBehaviour
{
    [SerializeField]
    MoleculeVerif moleculeData;
    [System.NonSerialized]
    public List<MolleculeJoint> moleculeJoints = new();
    [SerializeField]
    GameObject Camera;
    Transform cameraTransform;
    protected float distanceFromCamera;
    Vector3 CurrentCameraPos;
    Vector3 DiffTo;
    [System.NonSerialized]
     public bool followPos = false;
    [System.NonSerialized]
    public bool followRot = false;

    private TextMeshProUGUI textComponent;

    public GameObject Blaka,Lektiba;

    private void Start()
    {
        var inthis = "Molecule Demanded : \n" + moleculeData.Symbol + "\n composed of  :  \n";

        foreach(var atom in moleculeData.atoms)
        {
            inthis += "+"+atom.atom.symbol;
        }

        transform.position = new Vector3(1.126f, 0.724f, 1.553f);
        CurrentCameraPos = Camera.transform.position;
        cameraTransform = Camera.transform;
        distanceFromCamera = Vector3.Distance(transform.position, cameraTransform.position);
         textComponent = Lektiba.GetComponent<TextMeshProUGUI>();

        textComponent.text = inthis;




    }
    private void Update()
    {
        if (Camera.transform.position != CurrentCameraPos)
        {
            DiffTo= Camera.transform.position- CurrentCameraPos;
            CurrentCameraPos = Camera.transform.position;
            followPos = true;        
        }
        
    }
    private void LateUpdate()
    {
        if(followPos)
        {
            transform.position += DiffTo;
            followPos = false;
        }
        Vector3 targetPosition = cameraTransform.position + cameraTransform.forward * distanceFromCamera;
        transform.SetPositionAndRotation(targetPosition, Quaternion.LookRotation(transform.position - cameraTransform.position));
    }

    public void Verif()
    {
        if (moleculeJoints == null)
        {
            Debug.Log("moleculeJoints is null");
            return;
        }

        foreach (var atomCount in moleculeData.atoms)
        {
            int count = moleculeJoints.Count(mj => mj.mData.symbol == atomCount.atom.symbol);
            if (count !=atomCount.count)
            {
                //for (var i = gameObject.transform.childCount - 1; i >= 0; i--)
                //{
                //    if (gameObject.transform.GetChild(i))
                //        Destroy(gameObject.transform.GetChild(i).gameObject);
                //}
                textComponent.text = "EPIC FAIL";
                //Destroy(gameObject);
                return;
            }
        }
        textComponent.text = "EPIC SUCCESS you just created : "+moleculeData.name;
        return;
    }

    public void ActiveInfo()
    {
        Blaka.SetActive(true);
    }
    public void DesActiveInfo()
    {
        Blaka.SetActive(false);
    }


}