using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.XR.Interaction.Toolkit.Transformers;

public class attached : XRSocketInteractor
{
    public GameObject lena;
    MolleculeJoint joint;


    protected override void Start()
    {
        base.Start();
        lena = GameObject.Find("LENA");
        if(lena)
        { 
        transform.parent.parent = lena.transform;
        joint = transform.parent.GetComponent<MolleculeJoint>();
        }

    }
    protected override void OnSelectEntered(SelectEnterEventArgs args)
    {
        base.OnSelectEntered(args);
        joint.joints++;
        if (args.interactableObject.transform.GetChild(0) != null)
        {
            args.interactableObject.transform.GetChild(0).gameObject.SetActive(true);
            lena.GetComponent<Verify>().Verif();

        }

    }


}



