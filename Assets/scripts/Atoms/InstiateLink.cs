using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class InstiateLink : XRSimpleInteractable
{
    public GameObject link;
    idk leScript;
    Vector3 t;
    private void Start()
    {
        leScript = GetComponent<idk>();
    }
    protected override void OnSelectEntered(SelectEnterEventArgs args)
    {
        t=new Vector3(4.0f+transform.position.x,transform.position.y,transform.position.z);
        leScript.SpawnAtom(link, t);
    }
    
}
