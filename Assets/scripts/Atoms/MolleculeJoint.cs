using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.XR.Interaction.Toolkit;

public class MolleculeJoint : MonoBehaviour
{

    [System.NonSerialized]
    public int _numberJoints;
    [System.NonSerialized]
    public int joints = 0;
    public Molecule mData;
    int childCount = 0;
    private new Renderer renderer;

    public Verify verify;

    public GameObject childObject;


    private void Start()
    {
        verify = GameObject.Find("LENA").GetComponent<Verify>();
        verify.moleculeJoints.Add(this);
        Debug.Log("eli tzadet " + mData.symbol);


        gameObject.name = mData.fullName;
        _numberJoints = mData._maxLinks;
        renderer = GetComponent<Renderer>();
        renderer.material.color = mData.color;
        childCount = transform.childCount;
        if (childCount > _numberJoints)
            for (int i = _numberJoints; i < childCount; i++)
            {
                if (transform.GetChild(i).CompareTag("BondSocket"))
                    transform.GetChild(i).gameObject.SetActive(false);
            }

        TextMeshProUGUI textComponent = childObject.GetComponent<TextMeshProUGUI>();


        textComponent.text = "Symbol : " + mData.symbol + "\nFull Name : " + mData.fullName + "\nBonds Number : " + mData._maxLinks;

    }
    public void ActiveInfo()
    {
        transform.GetChild(4).gameObject.SetActive(true);
    }
    public void DesActiveInfo()
    {
        transform.GetChild(4).gameObject.SetActive(false);
    }
}
