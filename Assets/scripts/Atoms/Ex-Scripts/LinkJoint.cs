using System.Linq;
using Unity.VisualScripting;
using Unity.XR.CoreUtils;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.XR.Interaction.Toolkit.Transformers;

public class LinkJoint : XRGrabInteractable
{
    readonly int _numberJoints = 2;
    GameObject _collision=null;
    private bool _excuteCollision = false;
    MolleculeJoint script;
    Rigidbody body;

    private void Start()
    {

    }
    private void FixedUpdate()
    {
       if (!_excuteCollision)
           return;
        CreateJoint(_collision);
        AfterMath();
        

    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Mol"))

        { 
         script = collision.gameObject.GetComponent<MolleculeJoint>();
        
        if (GetComponents<FixedJoint>().Count() >= _numberJoints || !JointAvailable(collision.gameObject))
            return;
        if (script.joints < script._numberJoints)
        {
             _collision = collision.gameObject;
            _excuteCollision = true;
            }
        }

    }
    protected override void OnSelectEntered(SelectEnterEventArgs args)
    {
        base.OnSelectEntered(args);
        //OnObjectGrabbed();
        
    }

    protected override void OnSelectExited(SelectExitEventArgs args)
    {
        base.OnSelectExited(args);
        
       // OnObjectReleased();
        if (GetComponent<FixedJoint>() == null)
            return;
        var gg = GetComponent<FixedJoint>().connectedBody.gameObject.transform;
        Destroy(GetComponent<FixedJoint>());
        Debug.Log(gg.name);
        gg.SetParent(transform, true);

    }



    /***********************************************************************************************************************************************/
    public void OnObjectGrabbed()
    {
        GetComponent<Rigidbody>().isKinematic = false;

        var joints = GetComponents<FixedJoint>();
        //if (joints == null)
        //    return;
        //foreach (var joint in joints)
        //{
        //    if (joint.gameObject != gameObject)
        //        OnObjectGrabbed();
        //}

    }
    public void OnObjectReleased()
    {
        GetComponent<Rigidbody>().isKinematic = true;
        var joints = GetComponents<FixedJoint>();
        //if (joints == null)
        //    return;
        //foreach (var joint in joints)
        //{
        //    if (joint.gameObject != gameObject)

        //        OnObjectReleased();
        //}
    }
    /**********************************************************************************************************************************/
    private void CreateJoint(GameObject otherObject)
    {
        body = otherObject.GetComponent<Rigidbody>();
        var joint = transform.AddComponent<FixedJoint>();
        joint.connectedBody=body ;
       

    }
    private bool JointAvailable(GameObject otherobject)
    {
        var joints = transform.GetComponents<FixedJoint>();
        if (joints != null)
            foreach (var joint in joints)
            {
                if (otherobject == joint.connectedBody.gameObject)
                    return false;
            }
        return true;
    }
    private GameObject HasCotainer()
    {
        foreach (Transform child in transform)
        {
            if (child.name == "container")
                return child.gameObject;
        }
        return null;
    }

    private void AfterMath()
    {
        script.joints++;
        _collision = null;
        _excuteCollision = false;
        script = null;
    }

}




// if (_collision.transform.parent == null)
// {
//     //collision.transform.SetParent(gameObject.transform,true);
//     Debug.Log("EL NAME1 : " + _collision.name);

//     GameObject container = new("container");
//     Vector3 myScale = transform.localScale;
//     container.transform.localScale = new Vector3(1f / myScale.x, 1f / myScale.y, 1f / myScale.z);
//     container.transform.SetParent(transform, false);
//     _collision.transform.SetParent(container.transform);

// }
// else 
// {// gameObject.transform.SetParent(collision.transform.parent,true);
//     Debug.Log("EL NAME2 : " + _collision.gameObject.name);

//     _collision.transform.SetParent(HasCotainer().transform);
// }
// Debug.Log("EL NAME3 : " + _collision.gameObject.name);
// CreateJoint(_collision);