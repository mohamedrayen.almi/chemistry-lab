using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class GrabHandling : XRGrabInteractable
{
    private LinkJoint linkJoint;

    private void Start()
    {
        linkJoint = GetComponent<LinkJoint>();
    }
    protected override void OnSelectEntered(SelectEnterEventArgs args)
    {
        base.OnSelectEntered(args);
        OnObjectGrabbed();
    }

    protected override void OnSelectExited(SelectExitEventArgs args)
    {
        base.OnSelectExited(args);

        OnObjectReleased();
        if (GetComponent<FixedJoint>() == null)
            return;
        GetComponent<FixedJoint>().connectedBody.transform.SetParent(transform,true);
        Destroy(GetComponent<FixedJoint>());

    }



    /***********************************************************************************************************************************************/
    public void OnObjectGrabbed()
    {
       GetComponent<Rigidbody>().isKinematic = false;

        var joints = GetComponents<FixedJoint>();
        if (joints == null)
            return;
        foreach (var joint in joints)
        {
            if (joint.gameObject != gameObject)
                OnObjectGrabbed();
        }

    }
    public void OnObjectReleased()
    {
        GetComponent<Rigidbody>().isKinematic = true;
        var joints = GetComponents<FixedJoint>();
        if (joints == null)
            return;
        foreach (var joint in joints)
        {
            if (joint.gameObject != gameObject)

                OnObjectReleased();
        }
    }
}
