using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class AttachedBond : XRSocketInteractor
{
    public Verify verify;

    protected override void Start()
    {
        verify = GameObject.Find("LENA").GetComponent<Verify>();

    }
    protected override void OnSelectEntered(SelectEnterEventArgs args)
    {
        base.OnSelectEntered(args);
        if (args.interactableObject.transform.CompareTag("Mol"))
        {
            args.interactableObject.transform.GetComponent<MolleculeJoint>().joints++;
            
            foreach (Transform child in args.interactableObject.transform)
            {
                if (child.gameObject.activeSelf)
                {
                    child.gameObject.SetActive(false);
                    break;  
                }
            }
        }
        verify.Verif();

    }


}
