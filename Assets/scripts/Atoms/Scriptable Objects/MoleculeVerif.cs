using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewMoleculeVerif", menuName = "Molecule Verification")]

public class MoleculeVerif : ScriptableObject
{
    public string Symbol;
    [System.Serializable]
    public struct AtomCount
    {
        public Molecule atom;
        public int count;
    }

    public List<AtomCount> atoms;
}
