using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewMoleculeData", menuName = "Molecule Data")]
public class Molecule : ScriptableObject
{
    public string symbol;

    public string fullName;

    public Color color;

    public int _maxLinks;
}
