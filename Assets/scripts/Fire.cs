using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour
{
    public ParticleSystem fire;

    private ParticleSystemShapeType GetShapeType()
    {
        return fire.shape.shapeType;
    }

    // Start is called before the first frame update
    void Start(ParticleSystemShapeType shapeType)
    {
        fire = GetComponentInChildren<ParticleSystem>();
        shapeType = ParticleSystemShapeType.Donut;
        //fire.shape.meshShapeType = shapeType;
        //fire.shape.shapeType = shapeType;
    }

    // Update is called once per frame
    void Update()
    {
    }
    
}
