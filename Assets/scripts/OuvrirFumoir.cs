using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OuvrirFumoir : MonoBehaviour
{
    public GameObject g;
    public bool ouvert = false;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (!ouvert)
        {
            g.transform.rotation = Quaternion.identity;
        }
        else if (ouvert)
        {
            g.transform.rotation = Quaternion.AngleAxis(-90,Vector3.forward);
            
        }

    }
    
}
