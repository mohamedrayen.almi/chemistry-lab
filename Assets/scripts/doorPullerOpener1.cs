using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class doorPullerOpener1 : MonoBehaviour
{
    public bool SelectEntered;
    public GameObject a;
    public GameObject b;
    public bool ouvert = false;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (!ouvert)
        {
            a.transform.rotation = Quaternion.identity;

            b.SetActive(false);
        }
        else if (ouvert)
        {
            a.transform.rotation = Quaternion.AngleAxis(90, Vector3.up);
            b.SetActive(true);
        }
    }
    public void ouvrir()
    {
        ouvert = true;
    }
    public void fermer()
    {
        ouvert = false;
    }
    public void OPEN()
    {
        if (!SelectEntered)
        {
            ouvrir();
            SelectEntered = true;
            return;
        }
        else
        {
            fermer();
            SelectEntered = false;
            return;
        }
    }
}
