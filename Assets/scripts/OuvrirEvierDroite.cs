using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OuvrirEvierDroite : MonoBehaviour
{
    public GameObject a;
    public GameObject b;
    public bool ouvert = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!ouvert)
        {
            a.transform.rotation = Quaternion.identity;
            b.SetActive(false);
        }
        else if (ouvert)
        {
            a.transform.rotation = Quaternion.AngleAxis(-60, Vector3.right);
            b.SetActive(true);

        }
    }
}
