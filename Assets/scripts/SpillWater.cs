using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpillWater : MonoBehaviour
{
    public GameObject dropWater;
    public Color matColor;
    public RobinerOpen scriptRobnier;

    private Material newMaterial;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    void InstantiatePrefab()
    {
        // Instancie le pr�fab � la position (0, 0, 0) et avec une rotation de z�ro
        newMaterial = new Material(Shader.Find("Universal Render Pipeline/Lit"));

        dropWater.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
        newMaterial.color = matColor;

        dropWater.GetComponent<MeshRenderer>().material = newMaterial;
        Instantiate(dropWater, new Vector3(transform.position.x, transform.position.y + 0.005f, transform.position.z), Quaternion.identity);
    }
    // Update is called once per frame
    void Update()
    {

        if (scriptRobnier.isOpen)
        {
        InstantiatePrefab();
        }
    }
}
