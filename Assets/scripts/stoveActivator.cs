using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class stoveActivator : MonoBehaviour
{
    public bool ouvert;
    public GameObject  b;

    public bool SelectEntered;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!ouvert)
        {
            b.SetActive(false);
        }
        else if (ouvert)
        {
            b.SetActive(true);

        }
    }
    public void ouvrir()
    {
        ouvert = true;
    }
    public void fermer()
    {
        ouvert = false;
    }
    public void FireOn()
    {
        if (!SelectEntered)
        {
            ouvrir();
            SelectEntered = true;
            return;
        }
        else
        {
            fermer();
            SelectEntered = false;
            return;
        }
    }
}
