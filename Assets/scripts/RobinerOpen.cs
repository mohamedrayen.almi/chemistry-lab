using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobinerOpen : MonoBehaviour
{
    public bool isOpenRob;
    public bool isOpen;

    private Vector3 CloseState;
    private Vector3 OpenState;

    // Start is called before the first frame update
    private void Awake()
    {
        CloseState = transform.localPosition;
        OpenState = new Vector3(0f,0f, transform.localPosition.z) ;
    }
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(isOpenRob)
        {
           
            if (transform.localPosition.x < 0f && transform.localPosition.y > 0f)
            {
                transform.Translate(0.003f, -0.003f, 0f);
            }
            else
            {
                transform.localPosition = OpenState;
                isOpenRob = false;
            }
        }
        else
        {
            if (transform.localPosition.x > CloseState.x && transform.localPosition.y < CloseState.y)
            {
                transform.Translate(-0.0003f, 0.0003f, 0f);
            }
            else
            {
                transform.localPosition = CloseState;
                
            }
        }

        isOpen = transform.localPosition.x > CloseState.x && transform.localPosition.y < CloseState.y;

    }


    public void ChangeIsOpen()
    {
        isOpenRob = true;
    }
}
