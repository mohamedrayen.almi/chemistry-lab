using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyDrop : MonoBehaviour
{
    // Start is called before the first frame update
    public float time = 0.7f; 
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        time -= Time.deltaTime;
        if(time < 0f )
        {
            Destroy(gameObject);
        }
    }
}
