using System.Collections;
using System.Collections.Generic;
using UnityEditor.Rendering;
using UnityEngine;

public class TapBehaviour : MonoBehaviour
{
    // Start is called before the first frame update
    bool isPouring=false;
    private ParticleSystem streamPrefab;
    void Start()
    {
        streamPrefab = GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TogglePour()
    {
        isPouring = !isPouring;
        if (isPouring)
            streamPrefab.Play();
        else
            streamPrefab.Stop();
    }
}
