using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FillWater : MonoBehaviour
{
    public Shader baseShader;
    private Material material;

    // Start is called before the first frame update
    void Start()
    {
        Material mat = new Material(baseShader);
        GetComponent<MeshRenderer>().material = mat;
        material = GetComponent<MeshRenderer>().material;

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.tag.Equals("Water"))
        {
            Debug.Log("sddsv"); 
            Destroy(c.gameObject);
            material.SetFloat("_Fill", material.GetFloat("_Fill") + 0.005f);
            material.SetColor("_ColorLiquide", c.gameObject.GetComponent<MeshRenderer>().material.color);

        }
    }

}
