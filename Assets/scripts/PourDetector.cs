using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PourDetector : MonoBehaviour
{
    public int pourThreshold = 35;
    public Transform origin = null;
    public ParticleSystem streamPrefab;
    public Renderer liquid;
    private bool isPouring = false;
    public float quantity;
    public string liquidType;
    public float pHLevel;
    public Material droplet;
    Color[] pHTable = new Color[7];
    public float transitionDuration = 2.0f;
    bool colored = false;

    // Update is called once per frame
    private void Start()
    {
        streamPrefab.Stop();

        droplet= streamPrefab.GetComponent<Renderer>().material;

        // Basic (Left)
        pHTable[0] = new Color(0f, 0f, 1f);       // Blue
        pHTable[1] = new Color(0.29f, 0f, 0.51f);  // Indigo
        pHTable[2] = new Color(0.54f, 0.17f, 0.89f);// Violet

        // Neutral (Middle)
        pHTable[3] = new Color(0f, 1f, 0f);       // Green

        // Acidic (Right)
        pHTable[4] = new Color(1f, 1f, 0f);       // Yellow
        pHTable[5] = new Color(1f, 0.65f, 0f);    // Orange
        pHTable[6] = new Color(1f, 0f, 0f);       // Red
    }
    void Update()
    {
        quantity = liquid.material.GetFloat("_Fill");
        bool pourCheck = CalculatePourAngle() > pourThreshold;
        if (isPouring)
        {
            quantity-=0.001f;
            if (quantity < 0.5)
            {
                isPouring = false;
            }
        }
        if (isPouring != pourCheck) { 
            isPouring=  pourCheck;
            if(isPouring && quantity>0.5) {
                StartPour();
            }
            else
            {
                EndPour();
            }
        }
        if (quantity > 1) quantity = 1;
        liquid.material.SetFloat("_Fill", quantity);
    }


    Color MapPhToColor(float pH)
    {
        // Normalize the pH level to the range [0, 1]
        float normalizedPh = Mathf.Clamp01((pH - 0) / (14 - 0));

        // Determine the indices of the two closest colors
        int startIndex = Mathf.FloorToInt(normalizedPh * (pHTable.Length - 1));
        int endIndex = Mathf.CeilToInt(normalizedPh * (pHTable.Length - 1));

        // Use Color.Lerp to interpolate between the two colors
        return Color.Lerp(pHTable[startIndex], pHTable[endIndex], normalizedPh * (pHTable.Length - 1) - startIndex);
    }

    void AdjustpHLevel(string phType)
    {
        switch (phType)
        {
            case "Acid":
                if (pHLevel < 14)
                    pHLevel += 0.1f;
                break;
            case "Basic":
                if (pHLevel > 0)
                    pHLevel -= 0.1f;
                break;
            case "Neutral":
                if (pHLevel < 7) pHLevel += 0.1f;
                if (pHLevel > 7) pHLevel -= 0.1f;
                break;
        }
        switch (pHLevel)
        {
            case float value when value >= 0 && value <= 6:
                liquidType="Basic";
                streamPrefab.tag = "Basic";
                break;
            case float value when value > 6 && value <= 8:
                liquidType = "Neutral";
                streamPrefab.tag = "Neutral";
                break;
            case float value when value > 8 && value <= 14:
                liquidType = "Acid";
                streamPrefab.tag = "Acid";
                break;

        }
    }
    private void StartPour()
    {
        streamPrefab.Play();
    }

    private void EndPour()
    {
        streamPrefab.Stop();
    }

    private float CalculatePourAngle()
    {
        float yright = Mathf.Abs(transform.right.y * Mathf.Rad2Deg);
        float yforward = Mathf.Abs(transform.forward.y * Mathf.Rad2Deg);
        float pourAngle = Mathf.Max(yright, yforward);
        return pourAngle;
    }
    //private void OnParticleCollision(GameObject other)
    //{
    //    if (!isTransitioning)
    //    {
    //        isTransitioning = true;
    //        transitionStartTime = Time.time;

    //        TransitionColor(other.tag);
    //    }
    //    else
    //    {
    //        isTransitioning = false; // Stop the ongoing transition
    //    }
    //    FillContainer(other.tag);
    //}

    //private void TransitionColor(string particleTag)
    //{
    //    Color currentSideCol = liquid.material.GetColor("_SideColor");

    //    int startIndex;
    //    switch (particleTag)
    //    {
    //        case "Acid":
    //            startIndex = 0;
    //            break;
    //        case "Basic":
    //            startIndex = pHTable.Length - 1;
    //            break;
    //        case "Neutral":
    //            startIndex = pHTable.Length / 2;
    //            break;
    //        default:
    //            isTransitioning = false;
    //            return ; // Exit the method if the tag is not recognized
    //    }

    //    while (isTransitioning)
    //    {
    //        float elapsed = Time.time - transitionStartTime;
    //        float t = Mathf.Clamp01(elapsed / transitionDuration);

    //        int currentIndex = Mathf.FloorToInt((startIndex + t * pHTable.Length) % pHTable.Length);
    //        Color lerpedColor = Color.Lerp(currentSideCol, pHTable[currentIndex], t);
    //        ApplyColor(lerpedColor);

    //        if (t >= 1.0f || !isTransitioning)
    //        {
    //            isTransitioning = false;
    //        }

    //    }
    //}

    //private void FillContainer(string particleTag)
    //{
    //    bool fill=false;
    //    switch (particleTag)
    //    {
    //        case "Acid":
    //            fill = true;
    //            break;
    //        case "Basic":
    //            fill = true;
    //            break;
    //        case "Neutral":
    //            fill = true;
    //            break;
    //        default:
    //            return; // Exit the method if the tag is not recognized
    //    }
    //    if (fill)
    //    {
    //        quantity = liquid.material.GetFloat("_Fill");
    //        quantity += 0.005f;
    //        liquid.material.SetFloat("_Fill", quantity);
    //    }
    //}
    //private void ApplyColor(Color color)
    //{
    //    // Replace this with how you want to use the color in your project
    //    GetComponent<Renderer>().material.SetColor("_SideColor", color);
    //    GetComponent<Renderer>().material.SetColor("_TopColor", color);
    //}
    //private void OnParticleCollision(GameObject other)
    //{
    //    Color currentSideCol = liquid.material.GetColor("_SideColor");
    //    Color newTopCol = currentSideCol; // Start with the current color

    //    if (!other.tag.Equals(liquidType))
    //    {
    //        if (liquidType.Equals("Water"))
    //        {
    //            // Adjust color for water
    //            if (other.CompareTag("Red"))
    //            {
    //                newTopCol = Color.Lerp(currentSideCol, Color.red, 0.01f);
    //            }
    //            else if (other.CompareTag("Green"))
    //            {
    //                newTopCol = Color.Lerp(currentSideCol, Color.green, 0.01f);
    //            }
    //            else if (other.CompareTag("Blue"))
    //            {
    //                newTopCol = Color.Lerp(currentSideCol, Color.blue, 0.01f);
    //            }
    //        }
    //        else if (liquidType.Equals("Green"))
    //        {
    //            // Adjust color for green
    //            if (other.CompareTag("Red"))
    //            {
    //                newTopCol = Color.Lerp(currentSideCol, Color.red, 0.01f);
    //            }
    //            else if (other.CompareTag("Water"))
    //            {
    //                newTopCol = Color.Lerp(currentSideCol, Color.blue, 0.01f);
    //            }
    //            // No specific condition for Green, as it's already Green
    //        }
    //        else if (liquidType.Equals("Red"))
    //        {
    //            // Adjust color for red
    //            if (other.CompareTag("Green"))
    //            {
    //                newTopCol = Color.Lerp(currentSideCol, Color.green, 0.01f);
    //            }
    //            else if (other.CompareTag("Water"))
    //            {
    //                newTopCol = Color.Lerp(currentSideCol, Color.blue, 0.01f);
    //            }
    //            // No specific condition for Red, as it's already Red
    //        }

    //        print(currentSideCol);
    //        quantity = liquid.material.GetFloat("_Fill");
    //        quantity += 0.005f;
    //        liquid.material.SetFloat("_Fill", quantity);
    //        liquid.material.SetColor("_SideColor", newTopCol);
    //        liquid.material.SetColor("_TopColor", newTopCol);
    //    }

    //}
    private void OnParticleCollision(GameObject other)
    {
        Color currentSideCol = liquid.material.GetColor("_SideColor");
        //Color newTopCol = currentSideCol; // Start with the current color
        Color newTopCol;
        if (other.tag.Equals("Universal Indicator"))
        {
            if(!liquidType.Equals("Universal Indicator")) { 
            StartCoroutine(TransitionColor(MapPhToColor(pHLevel), 0.1f));
            colored = true;
            } 
        }
        else if(other.GetComponent<ParticleSystem>() != streamPrefab)
        {
            Renderer otherRenderer = other.GetComponent<Renderer>();
            if (otherRenderer != null)
            {
                AdjustpHLevel(other.tag);
                if (colored) { 
                newTopCol = MapPhToColor(pHLevel);
                    liquid.material.SetColor("_SideColor", newTopCol);
                    liquid.material.SetColor("_TopColor", newTopCol);
                    droplet.color = newTopCol;
                }
                quantity = liquid.material.GetFloat("_Fill");
                quantity += 0.005f;
                liquid.material.SetFloat("_Fill", quantity);
            }
        }
    }

    IEnumerator TransitionColor(Color targetColor, float transitionSpeed)
    {
        Color currentSideCol = liquid.material.GetColor("_SideColor");
        float elapsed = 0f;

        while (elapsed < 1f)
        {
            elapsed += Time.deltaTime * transitionSpeed;
            Color lerpedColor = Color.Lerp(currentSideCol, targetColor, elapsed);
            liquid.material.SetColor("_SideColor", lerpedColor);
            liquid.material.SetColor("_TopColor", lerpedColor);

            yield return null;
        }
    }

    //private GameObject CreateStream()
    //{
    //    GameObject streamObject = Instantiate(streamPrefab,origin.position,Quaternion.identity,transform);
    //    return streamObject;
    //}
}
