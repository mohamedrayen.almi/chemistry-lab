using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IronSlag : MonoBehaviour
{
    public ParticleSystem smokePuff;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "SulfiricAcid" && transform.localScale.z > 2)
        {
            transform.localScale = transform.localScale - new Vector3(0.01f, 0.01f, 0.01f);
        }
        if (transform.localScale.z < 2) {
            smokePuff.Stop();
            Destroy(this.gameObject); };
    }
}
