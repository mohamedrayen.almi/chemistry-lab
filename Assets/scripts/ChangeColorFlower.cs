using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColorFlower : MonoBehaviour
{
    //private Color color;
    public Shader ColorizeWater;
    public FillColoredWater scriptWaterDish;
    private Material material;
    private MainFlowerCollision scriptCollision;
    // Start is called before the first frame update
    void Start()
    {
        Material mat = new Material(ColorizeWater);

        GetComponent<MeshRenderer>().material = mat;
        material = GetComponent<MeshRenderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        scriptCollision = transform.parent.gameObject.GetComponent<MainFlowerCollision>();
        if (scriptWaterDish.WaterColor != Color.clear)
        {
            material.SetColor("_ColorLiquide", scriptWaterDish.WaterColor);
        }

        if(scriptCollision.isCollidiing && scriptWaterDish.GetComponent<MeshRenderer>().material.GetFloat("_Fill")> 0.5f ) 
        {
            material.SetFloat("_FillColor", material.GetFloat("_FillColor") - 0.0001f);
        }

    }

}
