using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class ShowTell : MonoBehaviour
{
    public GameObject uiCanvas; // Reference to the UI canvas
    private float initialZRotation; // Initial rotation along the Z-axis
    private bool isUIActive = false; // Track the UI state

    void Start()
    {
        initialZRotation = transform.localEulerAngles.z; // Store the initial rotation
    }

    void Update()
    {
        Debug.Log("el angle z : " + transform.localEulerAngles.z);

        
        if (transform.localEulerAngles.z<=270 && transform.localEulerAngles.z >= 180)
        {
            uiCanvas.SetActive(true); // Show the UI
            isUIActive = true;
        }
        else if (isUIActive && Mathf.Abs(transform.localEulerAngles.z - initialZRotation) > 35)
        {
            uiCanvas.SetActive(true);
        }
        else
        {
            uiCanvas.SetActive(false); 
            isUIActive = false;
        }
    }
}
